var chat = document.querySelector('#chat');
var input = document.querySelector('#chat-input');
var button = document.querySelector('#chat-button');

var socket = io.connect('http://localhost:3000');

socket.on('message', function (message) {
    var div = document.createElement('div');
    div.innerText = message;

    chat.appendChild(div);
});

function sendMessage(message) {
    socket.emit('message', message);
}

button.addEventListener('click', function () {
    var message = input.value;
    sendMessage(message);
});

