const express = require('express');
const socketIo = require('socket.io');
const http = require('http');

const app = express();
const server = http.Server(app);
const io = socketIo(server);


io.on('connection', (socket) => {
    socket.on('message', (message) => {
        io.emit('message', message);
    });
});

app.use(express.static('public'));

// app.get('/', (req, res) => {
//     res.send('Hello World!');
// });

server.listen(3000, () => {
    console.log('Example app listening on port 3000!');
});
